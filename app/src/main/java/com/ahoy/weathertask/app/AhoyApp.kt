package com.ahoy.weathertask.app

import androidx.multidex.MultiDexApplication
import androidx.work.*
import com.ahoy.base.di.utilsModule
import com.ahoy.data.di.*
import com.ahoy.data.other.MorningTemperatureWorker
import com.ahoy.weathertask.di.allFeatures
import com.orhanobut.hawk.Hawk
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

/**
 * Custom app class to initialize koin dependencies graph
 */
class AhoyApp : MultiDexApplication() {


    override fun onCreate() {
        super.onCreate()
        initHawk()
        initKoin()
        setUpDailyTemperatureNotification()
    }



    private fun initKoin() {
        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@AhoyApp)
            val modules = ArrayList(allFeatures)
            modules.addAll(listOf(keyValueStoreModule,databaseModule, utilsModule ,networkModule, repositoryModule , useCasesModule))
            modules(
                modules
            )
        }
    }

    private fun initHawk() {
        Hawk.init(this).build();
    }

    private fun setUpDailyTemperatureNotification(){
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED).build()

        val currentDate = Calendar.getInstance()
        val dueDate = Calendar.getInstance()
        dueDate.set(Calendar.HOUR_OF_DAY, 8)
        dueDate.set(Calendar.MINUTE, 0)
        dueDate.set(Calendar.SECOND, 0)
        if (dueDate.before(currentDate)) {
            dueDate.add(Calendar.HOUR_OF_DAY, 24)
        }

        val timeDiff = dueDate.timeInMillis - currentDate.timeInMillis

        val periodicWorkRequest =
            PeriodicWorkRequest.Builder(MorningTemperatureWorker::class.java, 1, TimeUnit.DAYS)
                .setInitialDelay(timeDiff, TimeUnit.MILLISECONDS)
                .setConstraints(constraints)
                .build()

        val workManager = WorkManager.getInstance(this)

        workManager.enqueueUniquePeriodicWork("morning_temp", ExistingPeriodicWorkPolicy.KEEP,periodicWorkRequest)
    }


}