package com.ahoy.weathertask.ui

import com.ahoy.base.baseEntities.BaseActivity
import com.ahoy.cities.navigation.CitiesNavigation
import com.ahoy.setting.navigation.SettingNavigation
import com.ahoy.weatherinfo.navigation.WeatherInfoNavigation
import com.ahoy.weathertask.R
import com.ahoy.weathertask.databinding.ActivityMainBinding

class MainScreen : BaseActivity<ActivityMainBinding>() {


    override fun getLayoutRes(): Int = R.layout.activity_main

    override fun getToolbarTitle(): Any? = null


    override fun onViewAttach() {
        super.onViewAttach()
        setUpClickListeners()
    }

    private fun setUpClickListeners() {
        mBinding.btnCurrentLocation.setOnClickListener {
            WeatherInfoNavigation().startCurrentWeatherInfoScreen()
        }
        mBinding.btnAllCities.setOnClickListener {
            CitiesNavigation().starAllCitiesScreen()
        }
        mBinding.btnSetting.setOnClickListener {
            SettingNavigation().starSettingScreen()
        }
        mBinding.btnFavouritesCities.setOnClickListener {
            CitiesNavigation().starFavCitiesScreen()
        }
    }

}