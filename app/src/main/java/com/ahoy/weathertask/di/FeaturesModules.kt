package com.ahoy.weathertask.di

import com.ahoy.cities.di.citiesModule
import com.ahoy.setting.di.settingModule
import com.ahoy.weatherinfo.di.weatherInfoModule


var allFeatures = listOf(weatherInfoModule,citiesModule, settingModule)
