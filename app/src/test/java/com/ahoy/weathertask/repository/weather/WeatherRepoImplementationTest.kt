package com.ahoy.weathertask.repository.weather

import android.content.Context
import com.ahoy.base.utils.NetworkHelper
import com.ahoy.data.database.entity.CityWeatherDto
import com.ahoy.data.keyValueStore.KeyValueStore
import com.ahoy.data.keyValueStore.entity.CurrentWeatherDto
import com.ahoy.data.other.ResultState
import com.ahoy.data.repository.weather.WeatherLocalDataSource
import com.ahoy.data.repository.weather.WeatherRemoteDataSource
import com.ahoy.data.repository.weather.WeatherRepoImplementation
import com.ahoy.weathertask.R
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldBeInstanceOf
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers
import org.mockito.ArgumentMatchers.anyDouble
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class WeatherRepoImplementationTest{

    private lateinit var weatherRepo: WeatherRepoImplementation

    @Mock
    private lateinit var localDataSource: WeatherLocalDataSource
    @Mock
    private lateinit var remoteDataSource: WeatherRemoteDataSource
    @Mock
    private lateinit var context: Context

    @Mock
    private lateinit var networkHelper: NetworkHelper


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        weatherRepo = WeatherRepoImplementation(context, networkHelper, localDataSource, remoteDataSource)
    }


    @Test
    fun testGetCurrentWeatherWhenConnectedAndCachedDataIsAvailable(): Unit = runBlocking {

        val lat = 31.211212112
        val lng = 31.211212112

        val result = Mockito.mock(CurrentWeatherDto::class.java)

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(true)

        Mockito.`when`(remoteDataSource.getCurrentWeather(lat, lng))
            .thenReturn(result)

        Mockito.`when`(localDataSource.getCurrentWeather())
            .thenReturn(result)

        val allFlowEmits = weatherRepo.getCurrentWeather(lat, lng).toList()

        Assert.assertEquals(3,allFlowEmits.size)

        allFlowEmits[0] shouldBeInstanceOf ResultState.Loading::class.java
        allFlowEmits[1] shouldBeInstanceOf ResultState.Success::class.java
        allFlowEmits[2] shouldBeInstanceOf ResultState.Success::class.java

        (allFlowEmits[1] as ResultState.Success).data shouldBeEqualTo result
        (allFlowEmits[2] as ResultState.Success).data shouldBeEqualTo result

        Mockito.verify(localDataSource, Mockito.times(2))
            .getCurrentWeather()

        Mockito.verify(localDataSource, Mockito.times(1))
            .updateCurrentWeather(result)

        Mockito.verify(remoteDataSource, Mockito.times(1))
            .getCurrentWeather(lat, lng)


    }


    @Test
    fun testGetCurrentWeatherWhenDisconnectedAndNotCachedData(): Unit = runBlocking {

        val lat = 31.211212112
        val lng = 31.211212112

        val error = "No internet connection"

        Mockito.`when`(context.getString(R.string.no_internet_connection))
            .thenReturn(error)

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(false)

        Mockito.`when`(localDataSource.getCurrentWeather())
            .thenReturn(null)

        val allEmits = weatherRepo.getCurrentWeather(lat,lng).toList()

        Assert.assertEquals(allEmits.size,2)

        allEmits[0] shouldBeInstanceOf ResultState.Loading::class.java
        allEmits[1] shouldBeInstanceOf ResultState.Error::class.java
        ( allEmits[1] as ResultState.Error ).exception.message shouldBeEqualTo error

        Mockito.verify(remoteDataSource, Mockito.never())
            .getCurrentWeather(lat, lng)

    }

    @Test
    fun testGetCurrentWeatherWhenDisconnectedAndCachedData(): Unit = runBlocking {

        val lat = 31.211212112
        val lng = 31.211212112

        val gameItem = Mockito.mock(CurrentWeatherDto::class.java)

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(false)

        Mockito.`when`(localDataSource.getCurrentWeather())
            .thenReturn(gameItem)

        val allEmits = weatherRepo.getCurrentWeather(lat, lng).toList()

        Assert.assertEquals(2,allEmits.size)

        allEmits[0] shouldBeInstanceOf ResultState.Loading::class.java
        allEmits[1] shouldBeInstanceOf ResultState.Success::class.java
        (allEmits[1] as ResultState.Success).data shouldBeEqualTo gameItem

        Mockito.verify(localDataSource, Mockito.times(1))
            .getCurrentWeather()


    }

}