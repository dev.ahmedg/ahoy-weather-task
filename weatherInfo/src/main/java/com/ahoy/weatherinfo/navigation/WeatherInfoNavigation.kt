package com.ahoy.weatherinfo.navigation

import android.os.Bundle
import com.ahoy.base.baseEntities.BaseNavigationManager
import com.ahoy.data.network.entities.City
import com.ahoy.weatherinfo.view.WeatherInfoActivity

class WeatherInfoNavigation : BaseNavigationManager() {

    fun startCurrentWeatherInfoScreen(){
        start(WeatherInfoActivity::class.java)
    }

    fun startCityWeatherInfoScreen(city: City){
        startWithBundle(WeatherInfoActivity::class.java, Bundle().apply {
            putParcelable(KEY_CITY,city)
        })
    }

    companion object{
        const val KEY_CITY = "city"
    }

}