package com.ahoy.weatherinfo.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.ahoy.base.baseEntities.BaseAdapter
import com.ahoy.weatherinfo.databinding.ItemDailyForecastBinding

class ForecastsAdapter: BaseAdapter<ForecastItemUiModel>() {


    override fun createBinding(parent: ViewGroup, viewType: Int): ViewDataBinding {
        return ItemDailyForecastBinding.inflate(LayoutInflater.from(parent.context),parent,false)
    }

    override fun bind(binding: ViewDataBinding, position: Int) {
        val item = getItem(position)
        (binding as ItemDailyForecastBinding).apply {
            model = item
        }
    }
}