package com.ahoy.weatherinfo.view

data class ForecastItemUiModel(

    val humidity:String,

    val pressure:String,
)
