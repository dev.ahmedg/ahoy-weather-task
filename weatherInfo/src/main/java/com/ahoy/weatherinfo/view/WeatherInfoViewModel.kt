package com.ahoy.weatherinfo.view

import android.app.Application
import android.location.Location
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ahoy.data.domian.*
import com.ahoy.data.network.entities.City
import com.ahoy.data.other.ResultState
import com.ahoy.weatherinfo.mapper.fromCityWeatherInfoDtoToUiModel
import com.ahoy.weatherinfo.mapper.fromCurrentWeatherInfoDtoToUiModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch


class WeatherInfoViewModel(private val city:City?, private val context:Application,
                           private val getTempUnitUseCase: GetTempUnitUseCase,
                           private val setLastUserLocationUseCase: UpdateLastUserLocationUseCase,
                           private val getCityWeatherInfoUseCase: GetCityWeatherInfoUseCase,
                           private val getCurrentWeatherInfoUseCase: GetCurrentWeatherInfoUseCase,
                           private val addCityToFavUseCase: AddCityToFavUseCase
): ViewModel() {


    // State flow act as a hot flow does not restart  with each new collector - it uses the same buffer(latest value) and it has initial value
    private val _infoStateFlow = MutableStateFlow<ResultState<WeatherInfoUiModel>>(ResultState.Loading)
    val infoSateFlow : StateFlow<ResultState<WeatherInfoUiModel>> = _infoStateFlow


    init {
        city?.let {
            getCityWeatherInfo()
        }
    }

    private fun getCityWeatherInfo() {
        viewModelScope.launch {
            getCityWeatherInfoUseCase(city?.id!!).map {
                when(it){
                    is ResultState.Success ->{
                        val data = it.data
                        ResultState.Success(fromCityWeatherInfoDtoToUiModel(context,getTempUnitUseCase().value,data!!))
                    }
                    is ResultState.Loading -> it
                    is ResultState.Error -> it
                    is ResultState.Empty -> it
                }
            } .catch {  } .collect {
                _infoStateFlow.value = it
            }
        }
    }

   fun getCurrentWeatherInfo(lat:Double,lng:Double) {
        viewModelScope.launch {
            getCurrentWeatherInfoUseCase(lat,lng).map {
                when(it){
                    is ResultState.Success ->{
                        val data = it.data
                        ResultState.Success(fromCurrentWeatherInfoDtoToUiModel(context,getTempUnitUseCase().value,data!!))
                    }
                    is ResultState.Loading -> it
                    is ResultState.Error -> it
                    is ResultState.Empty -> it
                }
            }.catch {  }.collect {
                _infoStateFlow.value = it
            }
        }
    }


    fun addCityToFav() {
        viewModelScope.launch {
            addCityToFavUseCase(city!!).catch {  } .collect {
                getCityWeatherInfo()
            }
        }
    }

    fun updateLastLocation(it: Location) {
        setLastUserLocationUseCase(it)
    }
}