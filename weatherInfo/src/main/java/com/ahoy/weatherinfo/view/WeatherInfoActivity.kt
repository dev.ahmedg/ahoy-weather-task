package com.ahoy.weatherinfo.view

import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.ahoy.base.baseEntities.BaseActivity
import com.ahoy.data.network.entities.City
import com.ahoy.data.other.ResultState
import com.ahoy.weatherinfo.R
import com.ahoy.weatherinfo.databinding.ActivityWeatherInfoBinding
import com.ahoy.weatherinfo.navigation.WeatherInfoNavigation
import com.blankj.utilcode.util.StringUtils
import com.yayandroid.locationmanager.LocationManager
import com.yayandroid.locationmanager.configuration.Configurations
import com.yayandroid.locationmanager.configuration.LocationConfiguration
import com.yayandroid.locationmanager.listener.LocationListener
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class WeatherInfoActivity : BaseActivity<ActivityWeatherInfoBinding>() , LocationListener {

    private val mCity by lazy { intent.getParcelableExtra(WeatherInfoNavigation.KEY_CITY) as City? }

    private val mViewModel by viewModel<WeatherInfoViewModel> { parametersOf(mCity) }

    private val mForecastsAdapter = ForecastsAdapter()

    override fun getLayoutRes(): Int = R.layout.activity_weather_info

    override fun getToolbarTitle(): Any? = if (mCity!=null) mCity!!.name else getString(R.string.current_location)


    override fun initViewModel() {
        super.initViewModel()
        mBinding.viewModel = mViewModel
    }
    private lateinit var locationManager: LocationManager

    override fun onCreate(savedInstanceState: Bundle?) {
        locationManager = LocationManager.Builder(applicationContext)
            .configuration(getLocationConfiguration())
            .activity(this)
            .notify(this)
            .build()
        super.onCreate(savedInstanceState)
    }

    override fun onViewAttach() {
        super.onViewAttach()
        setUpRv()
        observeData()
    }

    private fun getLocationConfiguration(): LocationConfiguration = Configurations.defaultConfiguration(
        StringUtils.getString(R.string.enable_location_rational_message),
        StringUtils.getString(R.string.location_gps_message))

    private fun observeData() {

        if (!isCity()) startLocationUpdates()

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED){
                mViewModel.infoSateFlow.collect{
                    when(it){
                        is ResultState.Loading->showLoading(mBinding.stateful)
                        is ResultState.Success->{
                            bindData(it.data!!)
                        }
                        is ResultState.Error-> showError(mBinding.stateful,it.exception.localizedMessage){
                            Toast.makeText(
                                this@WeatherInfoActivity,
                                "Retry is not implemented",
                                Toast.LENGTH_SHORT
                            ).show()}
                        else -> {}
                    }
                }
            }
        }

    }

    private fun bindData(data: WeatherInfoUiModel) {

        showContent(mBinding.stateful)

        mBinding.tvMinTemp.text = data.minTmp
        mBinding.tvMaxTemp.text = data.maxTmp
        mBinding.tvHumidity.text = data.humidity
        mBinding.tvPressure.text = data.pressure

        if (isCity()){
            mBinding.btnFavCity.isVisible = true
            mBinding.btnFavCity.setOnClickListener( if (data.isFavCity==true) null else View.OnClickListener {
                mViewModel.addCityToFav()
            })
            mBinding.btnFavCity.text = getString(if (data.isFavCity ==true) R.string.added_to_favourites else R.string.add_to_favourites)
        }
        mForecastsAdapter.submitList(data.forecasts)
    }

    private fun setUpRv() {
        mBinding.rv.adapter = mForecastsAdapter
    }

    private fun isCity() = mCity!=null


    override fun onDestroy() {
        locationManager.onDestroy()
        super.onDestroy()
    }

    override fun onPause() {
        locationManager.onPause()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        locationManager.onResume()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        locationManager.onActivityResult(requestCode, resultCode, data)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,  grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun startLocationUpdates(){
        locationManager.get()
    }

    override fun onProcessTypeChanged(processType: Int) {

    }

    override fun onLocationChanged(location: Location?) {
        location?.let {
            mViewModel.updateLastLocation(it)
            locationManager.cancel()
            mViewModel.getCurrentWeatherInfo(it.latitude,it.longitude)
        }

    }

    override fun onLocationFailed(type: Int) {

    }

    override fun onPermissionGranted(alreadyHadPermission: Boolean) {

    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

    }

    override fun onProviderEnabled(provider: String?) {

    }

    override fun onProviderDisabled(provider: String?) {

    }

}