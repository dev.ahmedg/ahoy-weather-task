package com.ahoy.weatherinfo.view

data class WeatherInfoUiModel(

    var minTmp:String,

    var maxTmp:String,

    var humidity:String,

    var pressure:String,

    var forecasts: List<ForecastItemUiModel>,

    var isFavCity :Boolean ? = null

)
