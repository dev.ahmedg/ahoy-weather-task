package com.ahoy.weatherinfo.di




import com.ahoy.data.network.entities.City
import com.ahoy.weatherinfo.view.WeatherInfoViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val weatherInfoModule = module {


    viewModel {(city: City) -> WeatherInfoViewModel(city,androidApplication(),get(),get(),get(),get(),get()) }
}