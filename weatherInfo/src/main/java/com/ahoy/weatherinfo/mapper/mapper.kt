package com.ahoy.weatherinfo.mapper

import android.content.Context
import com.ahoy.data.database.entity.CityWeatherDto
import com.ahoy.data.keyValueStore.KeyValueStore
import com.ahoy.data.keyValueStore.entity.CurrentWeatherDto
import com.ahoy.data.repository.weather.WeatherRepo
import com.ahoy.weatherinfo.R
import com.ahoy.weatherinfo.view.ForecastItemUiModel
import com.ahoy.weatherinfo.view.WeatherInfoUiModel

 fun fromCurrentWeatherInfoDtoToUiModel(context: Context, unitType:String, weather: CurrentWeatherDto):WeatherInfoUiModel{
    val minTemp =context.getString(R.string.temp_placeholder,weather.weatherInfo.tempMin,unitType)
    val maxTemp =context.getString(R.string.temp_placeholder,weather.weatherInfo.tempMax,unitType)
    val humidity =context.getString(R.string.temp_placeholder,weather.weatherInfo.humidity,unitType)
    val pressure =context.getString(R.string.temp_placeholder,weather.weatherInfo.pressure,unitType)

    return WeatherInfoUiModel(minTemp,maxTemp,humidity,pressure,weather.forecasts.map { ForecastItemUiModel(
        context.getString(R.string.temp_placeholder,it.humidity,unitType),
        context.getString(R.string.temp_placeholder,it.pressure,unitType),
    ) })
}

 fun fromCityWeatherInfoDtoToUiModel(context: Context, unitType:String, weather: CityWeatherDto):WeatherInfoUiModel{
    val minTemp =context.getString(R.string.temp_placeholder,weather.weatherInfo.tempMin,unitType)
    val maxTemp =context.getString(R.string.temp_placeholder,weather.weatherInfo.tempMax,unitType)
    val humidity =context.getString(R.string.temp_placeholder,weather.weatherInfo.humidity,unitType)
    val pressure =context.getString(R.string.temp_placeholder,weather.weatherInfo.pressure,unitType)

    return WeatherInfoUiModel(minTemp,maxTemp,humidity,pressure,weather.forecasts.map { ForecastItemUiModel(
        context.getString(R.string.temp_placeholder,it.humidity,unitType),
        context.getString(R.string.temp_placeholder,it.pressure,unitType),
    ) },weather.isFavCity)
}

