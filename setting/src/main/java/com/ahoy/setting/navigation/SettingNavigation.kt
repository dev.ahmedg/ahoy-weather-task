package com.ahoy.setting.navigation

import com.ahoy.base.baseEntities.BaseNavigationManager
import com.ahoy.setting.view.SettingActivity

class SettingNavigation : BaseNavigationManager() {

    fun starSettingScreen(){
        start(SettingActivity::class.java)
    }


}