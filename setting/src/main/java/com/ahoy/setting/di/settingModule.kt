package com.ahoy.setting.di





import com.ahoy.setting.view.SettingViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val settingModule = module {

    viewModel { SettingViewModel(get(),get()) }

}