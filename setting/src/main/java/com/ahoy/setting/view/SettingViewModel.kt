package com.ahoy.setting.view


import androidx.lifecycle.ViewModel
import com.ahoy.data.domian.GetTempUnitUseCase
import com.ahoy.data.domian.SetTempUnitUseCase
import com.ahoy.data.keyValueStore.TempUnitType


class SettingViewModel(getTempUnitUseCase: GetTempUnitUseCase, private val  setTempUnitUseCase: SetTempUnitUseCase): ViewModel() {

    var tempUnitLiveData  = getTempUnitUseCase()


    fun updateTempUnit(unitType: TempUnitType) {
        setTempUnitUseCase(unitType)
        tempUnitLiveData = unitType
    }


}