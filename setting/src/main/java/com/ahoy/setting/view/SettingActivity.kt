package com.ahoy.setting.view

import com.ahoy.base.baseEntities.BaseActivity
import com.ahoy.data.keyValueStore.TempUnitType
import com.ahoy.setting.R
import com.ahoy.setting.databinding.ActivitySettingBinding
import com.llollox.androidtoggleswitch.widgets.ToggleSwitch
import org.koin.androidx.viewmodel.ext.android.viewModel


class SettingActivity : BaseActivity<ActivitySettingBinding >() {


    private val mViewModel by viewModel<SettingViewModel>()


    override fun getLayoutRes(): Int = R.layout.activity_setting

    override fun getToolbarTitle(): Any  = R.string.setting

    override fun onViewAttach() {
        super.onViewAttach()
        observeData()
        setClickListeners()
    }

    private fun setClickListeners() {
        mBinding.toggleView.onChangeListener =object: ToggleSwitch.OnChangeListener {
            override fun onToggleSwitchChanged(position: Int) {
               mViewModel.updateTempUnit(if (position ==0) TempUnitType.Celsius else TempUnitType.Fahrenheit)
            }
        }
    }

    private fun observeData() {

        mBinding.toggleView.setCheckedPosition(if (mViewModel.tempUnitLiveData == TempUnitType.Celsius ) 0 else 1)
    }



}