# Ahoy weather
This repository is based on a modularized MVVM and clean architecture with the usage of Use cases pattern and the repository pattern

## Modules
 1. Base module : contains superclasses for the common components (activity , fragment ,...) and other utils
 2. Data module : contains both persistance and network layers  repositories and use cases 
 3. Features modules : contain view , view models and usecase (Cities - Weather info - setting)
 4. app module : contains the app class and the app di module plus the entry point of the app .
 
 ## Stack
 1. Kotlin.
 2. Coroutines with flow.
 3. Live data.
 4. MVVM with clean arch (Depency inversion principle) , use cases and repositories
 5. Room database
 6. Retrofit
 7. Koin (Dependency injector)
 
 ## Unit tests
 Mainly mockito and other helper libs are used to provide fake dependencies in unit testing.

 1. Junit4 
 2. Junit Asserttions
 4. Mockito.
 5. kluent.

## Build environments
Uat , staging and production envs have been added as a phase flavor

## Daily weather temprature notification 
Work manager has been used due to it`s benefits with battery usage optimization and respectining doze mode , also it has a coroutine worker that supports suspend functions

### The source code has one tested class only : WeatherRepoImplementationTest
