package com.ahoy.base.di

import com.ahoy.base.utils.JsonAssetManager
import com.ahoy.base.utils.NetworkHelper
import org.koin.dsl.module

/**
 * Koin utils module
 */
val utilsModule = module {


    single { NetworkHelper() }

    single { JsonAssetManager(get()) }
}
