package com.ahoy.base.utils

import com.blankj.utilcode.util.NetworkUtils

open class NetworkHelper {

    open fun isConnected()= NetworkUtils.isConnected()

}