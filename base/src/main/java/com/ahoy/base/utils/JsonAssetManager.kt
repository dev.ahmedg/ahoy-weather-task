package com.ahoy.base.utils

import android.content.Context
import android.util.Log
import java.io.*

class JsonAssetManager(val context: Context) {


    inline fun <reified T>readFileAsArray(strFileName: String): List<T>? {

        return jsonAssetFileToArr(context,strFileName)
    }
}