package com.ahoy.cities.di




import com.ahoy.cities.view.allCities.AllCitiesViewModel
import com.ahoy.cities.view.favCities.FavCitiesViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val citiesModule = module {

    viewModel { AllCitiesViewModel(get()) }

    viewModel { FavCitiesViewModel(get()) }
}