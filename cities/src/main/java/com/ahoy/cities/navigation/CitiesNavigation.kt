package com.ahoy.cities.navigation

import com.ahoy.base.baseEntities.BaseNavigationManager
import com.ahoy.cities.view.allCities.AllCitiesActivity
import com.ahoy.cities.view.favCities.FavCitiesActivity
import com.ahoy.weatherinfo.view.WeatherInfoActivity

class CitiesNavigation : BaseNavigationManager() {

    fun starAllCitiesScreen(){
        start(AllCitiesActivity::class.java)
    }

    fun starFavCitiesScreen(){
        start(FavCitiesActivity::class.java)
    }


}