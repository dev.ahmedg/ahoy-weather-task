package com.ahoy.cities.view.favCities

import com.ahoy.base.baseEntities.BaseActivity
import com.ahoy.cities.R
import com.ahoy.cities.view.common.CitiesAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel
import android.widget.Toast
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.ahoy.cities.databinding.ActivityFavCitiesBinding
import com.ahoy.data.other.ResultState
import com.ahoy.weatherinfo.navigation.WeatherInfoNavigation
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


class FavCitiesActivity : BaseActivity<ActivityFavCitiesBinding>() {


    private val mViewModel by viewModel<FavCitiesViewModel>()

    private val mAdapter = CitiesAdapter{
        WeatherInfoNavigation().startCityWeatherInfoScreen(it)
    }

    override fun getLayoutRes(): Int = R.layout.activity_fav_cities

    override fun getToolbarTitle(): Any  = R.string.favourites_cities

    override fun onViewAttach() {
        super.onViewAttach()
        setUpRv()
        observeData()
    }

    private fun observeData() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED){
                mViewModel.listStateFlow.collect{
                    when(it){
                        is ResultState.Loading->showLoading(mBinding.stateful)
                        is ResultState.Success->{
                            showContent(mBinding.stateful)
                            mAdapter.submitList(it.data)
                        }
                        is ResultState.Error-> showError(mBinding.stateful,it.exception.localizedMessage){
                            Toast.makeText(
                                this@FavCitiesActivity,
                                "Retry is not implemented",
                                Toast.LENGTH_SHORT
                            ).show()}
                        else -> showEmpty(mBinding.stateful)
                    }
                }
            }
        }

    }



    private fun setUpRv() {
        mBinding.rv.adapter = mAdapter
    }
}