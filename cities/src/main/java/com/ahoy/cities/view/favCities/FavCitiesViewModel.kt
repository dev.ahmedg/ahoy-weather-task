package com.ahoy.cities.view.favCities

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ahoy.cities.view.common.CityItemUiModel
import com.ahoy.data.domian.GetFavCitesUseCase
import com.ahoy.data.other.ResultState
import kotlinx.coroutines.flow.*


class FavCitiesViewModel(private val getFavCitesUseCase: GetFavCitesUseCase): ViewModel() {


    val listStateFlow : StateFlow<ResultState<List<CityItemUiModel>>> = getFavCitesUseCase().map {
        when(it){
            is ResultState.Success ->{
                val data = it.data
                ResultState.Success(data?.map { CityItemUiModel(it,it.name!!) })
            }
            is ResultState.Loading -> it
            is ResultState.Error -> it
            is ResultState.Empty -> it
        }
    }.stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000),ResultState.Loading)



}