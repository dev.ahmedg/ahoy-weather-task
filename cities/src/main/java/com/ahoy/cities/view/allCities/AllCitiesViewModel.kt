package com.ahoy.cities.view.allCities

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ahoy.cities.view.common.CityItemUiModel
import com.ahoy.data.domian.SearchCitesUseCase
import com.ahoy.data.other.ResultState
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch


class AllCitiesViewModel(private val searchCitesUseCase: SearchCitesUseCase): ViewModel() {


    private val _searchResultsLiveData = MutableLiveData<ResultState<List<CityItemUiModel>>>()
    val searchResultsLiveData : LiveData<ResultState<List<CityItemUiModel>>> = _searchResultsLiveData


    fun search(query:String) {
        viewModelScope.launch {
            searchCitesUseCase(query).map {
                when(it){
                    is ResultState.Success ->{
                        val data = it.data
                        ResultState.Success(data?.map { CityItemUiModel(it,it.name!!) })
                    }
                    is ResultState.Loading -> it
                    is ResultState.Error -> it
                    is ResultState.Empty -> it
                }
            }.collect {
                _searchResultsLiveData.value = it
            }
        }
    }


}