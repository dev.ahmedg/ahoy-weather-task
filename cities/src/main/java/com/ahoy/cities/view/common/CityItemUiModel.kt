package com.ahoy.cities.view.common

import com.ahoy.data.network.entities.City

data class CityItemUiModel(
    val entity:City,
    val name:String
)