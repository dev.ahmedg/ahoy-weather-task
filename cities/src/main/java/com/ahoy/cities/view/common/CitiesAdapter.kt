package com.ahoy.cities.view.common

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.ahoy.base.baseEntities.BaseAdapter
import com.ahoy.cities.databinding.ItemCityBinding
import com.ahoy.data.network.entities.City

class CitiesAdapter(private val onItemClick :(City)->Unit): BaseAdapter<CityItemUiModel>() {


    override fun createBinding(parent: ViewGroup, viewType: Int): ViewDataBinding {
        return ItemCityBinding.inflate(LayoutInflater.from(parent.context),parent,false)
    }

    override fun bind(binding: ViewDataBinding, position: Int) {
        val item = getItem(position)
        (binding as ItemCityBinding).apply {
            model = item
            onClick = View.OnClickListener {
                onItemClick(item.entity)
            }
        }
    }
}