package com.ahoy.cities.view.allCities

import com.ahoy.base.baseEntities.BaseActivity
import com.ahoy.cities.R
import com.ahoy.cities.databinding.ActivityAllCitiesBinding
import com.ahoy.cities.view.common.CitiesAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import com.ahoy.data.other.ResultState
import com.ahoy.weatherinfo.navigation.WeatherInfoNavigation
import com.blankj.utilcode.util.KeyboardUtils


class AllCitiesActivity : BaseActivity<ActivityAllCitiesBinding>() {


    private val mViewModel by viewModel<AllCitiesViewModel>()

    private val mAdapter = CitiesAdapter{
        WeatherInfoNavigation().startCityWeatherInfoScreen(it)
    }

    override fun getLayoutRes(): Int = R.layout.activity_all_cities

    override fun getToolbarTitle(): Any  = R.string.all_cities

    override fun onViewAttach() {
        super.onViewAttach()
        setUpRv()
        setUpInputs()
        observeData()
    }

    private fun observeData() {
        mViewModel.searchResultsLiveData.observe(this){
            when(it){
                is ResultState.Loading->showLoading(mBinding.stateful)
                is ResultState.Success->{
                    showContent(mBinding.stateful)
                    mAdapter.submitList(it.data)
                }
                is ResultState.Error-> showError(mBinding.stateful,it.exception.localizedMessage){
                    Toast.makeText(
                        this,
                        "Retry is not implemented",
                        Toast.LENGTH_SHORT
                    ).show()}
                else -> {}
            }
        }
    }

    private fun setUpInputs() {
        mBinding.inputSearch.setOnEditorActionListener(OnEditorActionListener { v, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                KeyboardUtils.hideSoftInput(v)
                mViewModel.search(v.text.toString())
                return@OnEditorActionListener true
            }
            false
        })
    }

    private fun setUpRv() {
        mBinding.rv.adapter = mAdapter
    }
}