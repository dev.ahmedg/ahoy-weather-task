package com.ahoy.data.domian

import android.location.Location
import com.ahoy.data.repository.weather.WeatherRepo


open class UpdateLastUserLocationUseCase constructor(private val repository: WeatherRepo) {

     operator fun invoke(location: Location) = repository.setLastLocation(location)

}