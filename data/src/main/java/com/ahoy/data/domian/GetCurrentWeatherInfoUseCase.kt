package com.ahoy.data.domian

import com.ahoy.data.repository.weather.WeatherRepo


open class GetCurrentWeatherInfoUseCase constructor(private val repository: WeatherRepo) {

    suspend operator fun invoke(lat:Double,lng:Double) = repository.getCurrentWeather(lat,lng)

}