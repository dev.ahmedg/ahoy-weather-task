package com.ahoy.data.domian

import com.ahoy.data.repository.cities.CitiesRepo


open class SearchCitesUseCase constructor(private val repository: CitiesRepo) {

     operator fun invoke(query:String) = repository.searchCities(query)

}