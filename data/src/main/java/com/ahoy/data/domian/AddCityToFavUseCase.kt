package com.ahoy.data.domian

import com.ahoy.data.network.entities.City
import com.ahoy.data.repository.cities.CitiesRepo


open class AddCityToFavUseCase constructor(private val repository: CitiesRepo) {

    suspend operator fun invoke(city: City) = repository.addFavCity(city)

}