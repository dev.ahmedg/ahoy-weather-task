package com.ahoy.data.domian

import com.ahoy.data.keyValueStore.TempUnitType
import com.ahoy.data.repository.weather.WeatherRepo


open class SetTempUnitUseCase constructor(private val repository: WeatherRepo) {

     operator fun invoke(type : TempUnitType) = repository.setTempUnit(type)

}