package com.ahoy.data.domian

import com.ahoy.data.repository.weather.WeatherRepo


open class GetTempUnitUseCase constructor(private val repository: WeatherRepo) {

    operator fun invoke() = repository.getTempUnit()

}