package com.ahoy.data.domian

import com.ahoy.data.repository.weather.WeatherRepo


open class GetCityWeatherInfoUseCase constructor(private val repository: WeatherRepo) {

    suspend operator fun invoke(id:Int) = repository.getCityWeather(id)

}