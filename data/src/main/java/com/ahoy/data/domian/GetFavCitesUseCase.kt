package com.ahoy.data.domian

import com.ahoy.data.repository.cities.CitiesRepo


open class GetFavCitesUseCase constructor(private val repository: CitiesRepo) {

    operator fun invoke() = repository.getFavCities()

}