package com.ahoy.data.domian

import com.ahoy.data.repository.weather.WeatherRepo


open class GetLastUserLocationUseCase constructor(private val repository: WeatherRepo) {

     operator fun invoke() = repository.getLastLocation()

}