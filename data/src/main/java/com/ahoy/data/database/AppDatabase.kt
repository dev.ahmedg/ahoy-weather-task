package com.ahoy.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.ahoy.base.utils.fromJson
import com.ahoy.base.utils.fromJsonArray
import com.ahoy.base.utils.toJson
import com.ahoy.base.utils.toJsonArray
import com.ahoy.data.database.dao.CityWeatherDao
import com.ahoy.data.database.entity.CityWeatherDto
import com.ahoy.data.network.entities.ForecastItem
import com.ahoy.data.network.entities.WeatherMainData

/**
 * Room database class
 */
@Database(
    entities = [CityWeatherDto::class],
    version = 1
)
@TypeConverters(DataConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun cityWeatherDao() : CityWeatherDao


}

class DataConverter{

    @TypeConverter
    fun toForecastsList(json: String?): List<ForecastItem>? = fromJsonArray(json)

    @TypeConverter
    fun toForecastsJson(data : List<ForecastItem>): String? = toJsonArray(data)

    @TypeConverter
    fun toWeatherMainData(json: String?): WeatherMainData? = fromJson(json)

    @TypeConverter
    fun toWeatherMainJson(data : WeatherMainData): String? = toJson(data)


}
