package com.ahoy.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ahoy.data.database.entity.CityWeatherDto

@Dao
interface CityWeatherDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(model: CityWeatherDto)

    @Query("DELETE FROM city_weather WHERE cityId=:cityId")
    suspend fun clearCityForecasts(cityId: Int)

    @Query("SELECT * FROM city_weather WHERE cityId = :cityId")
    suspend fun getByCity(cityId: Int): CityWeatherDto?


}