package com.ahoy.data.database.entity

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.ahoy.data.network.entities.ForecastItem
import com.ahoy.data.network.entities.WeatherMainData

@Entity(tableName = "city_weather")
class CityWeatherDto(

    @PrimaryKey
    val cityId : Int ,

    var weatherInfo: WeatherMainData,

    var forecasts: List<ForecastItem>,

){

    @Ignore
    var isFavCity :Boolean ? = null

}
