package com.ahoy.data.other

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.ahoy.data.R
import com.ahoy.data.repository.weather.WeatherLocalDataSource
import com.ahoy.data.repository.weather.WeatherRemoteDataSource
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class MorningTemperatureWorker(private val context: Context, params: WorkerParameters) : CoroutineWorker(context,params),KoinComponent {

    private val weatherLocal by inject<WeatherLocalDataSource>()
    private val weatherRemote by inject<WeatherRemoteDataSource>()

    override suspend fun doWork(): Result {
        val lastUserLocation = weatherLocal.getLastLocation()
        if (lastUserLocation!=null){
            val data = weatherRemote.getCurrentWeather(lastUserLocation.latitude,lastUserLocation.longitude)

            val tempUnit = weatherLocal.getTempUnit().value
            sendNotification(context.getString(R.string.today_temp),context.getString(R.string.temp_placeholder,data.weatherInfo.temp,tempUnit))
        }
        return Result.success()
    }


    private fun sendNotification(title: String, message: String) {
        val notificationManager =
            applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel =
                NotificationChannel("default", "Default", NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }
        val notification: NotificationCompat.Builder = NotificationCompat.Builder(
            applicationContext,
            "default"
        )
            .setContentTitle(title)
            .setContentText(message)
            .setSmallIcon(R.mipmap.ic_launcher)
        notificationManager.notify(1, notification.build())
    }

}