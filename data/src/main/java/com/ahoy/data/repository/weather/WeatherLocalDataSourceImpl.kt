package com.ahoy.data.repository.weather

import android.location.Location
import com.ahoy.data.database.AppDatabase
import com.ahoy.data.database.entity.CityWeatherDto
import com.ahoy.data.keyValueStore.KeyValueStore
import com.ahoy.data.keyValueStore.TempUnitType
import com.ahoy.data.keyValueStore.entity.CurrentWeatherDto


class WeatherLocalDataSourceImpl(private val keyValueStore: KeyValueStore , private val appDatabase: AppDatabase) : WeatherLocalDataSource {



    override suspend fun getCurrentWeather(): CurrentWeatherDto? {
        return keyValueStore.getCurrentWeather()
    }

    override suspend fun updateCurrentWeather(data: CurrentWeatherDto) {
        keyValueStore.updateCurrentWeather(data)
    }

    override suspend fun getCityWeather(cityId: Int): CityWeatherDto? {
        return appDatabase.cityWeatherDao().getByCity(cityId)
    }

    override suspend fun updateCityWeather(data: CityWeatherDto) {
        appDatabase.cityWeatherDao().insert(data)
    }

    override  fun setTempUnit(type: TempUnitType) {
        keyValueStore.setTempUnitType(type)
    }

    override  fun getTempUnit(): TempUnitType {
        return keyValueStore.getTempUnitType()
    }

    override fun setLastLocation(location: Location) {
        keyValueStore.setLastLocation(location)
    }

    override fun getLastLocation(): Location? {
        return keyValueStore.getLastLocation()
    }

    override suspend fun isFavCity(id: Int): Boolean {
        return keyValueStore.isFavCity(id)
    }


}