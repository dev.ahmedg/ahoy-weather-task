package com.ahoy.data.repository.weather

import android.location.Location
import com.ahoy.data.database.entity.CityWeatherDto
import com.ahoy.data.keyValueStore.TempUnitType
import com.ahoy.data.keyValueStore.entity.CurrentWeatherDto
import com.ahoy.data.other.ResultState
import kotlinx.coroutines.flow.Flow


interface WeatherLocalDataSource {


    suspend fun getCurrentWeather():CurrentWeatherDto?

    suspend fun updateCurrentWeather(data : CurrentWeatherDto)

    suspend fun getCityWeather(cityId:Int):CityWeatherDto?

    suspend fun updateCityWeather(data: CityWeatherDto)

    fun setTempUnit(type : TempUnitType)

    fun getTempUnit() : TempUnitType

    fun setLastLocation(location: Location)

    fun getLastLocation(): Location?

    suspend fun isFavCity(id:Int):Boolean

}

interface WeatherRemoteDataSource {

    suspend fun getCurrentWeather(lat:Double, lng:Double): CurrentWeatherDto

    suspend fun getCityWeather(cityId:Int): CityWeatherDto

}


interface WeatherRepo {

    suspend fun getCurrentWeather(lat:Double,lng:Double): Flow<ResultState<CurrentWeatherDto>>

    suspend fun getCityWeather(cityId:Int): Flow<ResultState<CityWeatherDto>>

    fun setTempUnit(type : TempUnitType)

    fun getTempUnit() : TempUnitType

    fun setLastLocation(location: Location)

    fun getLastLocation(): Location?
}