package com.ahoy.data.repository.weather

import android.content.Context
import android.location.Location
import com.ahoy.base.utils.NetworkHelper
import com.ahoy.data.database.entity.CityWeatherDto
import com.ahoy.data.keyValueStore.TempUnitType
import com.ahoy.data.keyValueStore.entity.CurrentWeatherDto
import com.ahoy.data.other.RemoteLocalBoundResource
import com.ahoy.data.other.ResultState
import kotlinx.coroutines.flow.Flow

class WeatherRepoImplementation(
    private val context:Context,
    private val networkHelper: NetworkHelper,
    private val localDataSource: WeatherLocalDataSource,
    private val remoteDataSource: WeatherRemoteDataSource,
) : WeatherRepo {


    override suspend fun getCurrentWeather(lat: Double, lng: Double): Flow<ResultState<CurrentWeatherDto>> {
        return RemoteLocalBoundResource(context,networkHelper,
            remoteCall = { remoteDataSource.getCurrentWeather(lat,lng)},
            localCall = {localDataSource.getCurrentWeather()},
            saveLocal = { localDataSource.updateCurrentWeather(it)}).asFlow()
    }

    override suspend fun getCityWeather(cityId: Int): Flow<ResultState<CityWeatherDto>> {
        val isFav = localDataSource.isFavCity(cityId)

        return RemoteLocalBoundResource(context,networkHelper,
            remoteCall = { remoteDataSource.getCityWeather(cityId)},
            localCall = { localDataSource.getCityWeather(cityId)?.apply { isFavCity = isFav } },
            saveLocal = { localDataSource.updateCityWeather(it)}).asFlow()
    }

    override  fun setTempUnit(type: TempUnitType) {
        localDataSource.setTempUnit(type)
    }

    override  fun getTempUnit(): TempUnitType {
        return localDataSource.getTempUnit()
    }

    override fun setLastLocation(location: Location) {
        localDataSource.setLastLocation(location)
    }

    override fun getLastLocation(): Location? {
        return localDataSource.getLastLocation()
    }


}