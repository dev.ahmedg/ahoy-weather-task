package com.ahoy.data.repository.weather

import com.ahoy.data.database.entity.CityWeatherDto
import com.ahoy.data.keyValueStore.entity.CurrentWeatherDto
import com.ahoy.data.network.RetrofitExecutor
import com.ahoy.data.network.api.WeatherApiService


class WeatherRemoteDataSourceImpl(private val retrofitExecutor: RetrofitExecutor, private val apiService: WeatherApiService) :
    WeatherRemoteDataSource {


    override suspend fun getCurrentWeather(lat: Double, lng: Double): CurrentWeatherDto {
        val mainData = retrofitExecutor.makeRequest { apiService.getCurrentWeatherInfoByCoord(lat, lng) }.main!!
        val forecasts = retrofitExecutor.makeRequest { apiService.getWeatherForecastByCoord(lat, lng) }.list!!
        return CurrentWeatherDto(mainData,forecasts)
    }

    override suspend fun getCityWeather(cityId: Int): CityWeatherDto {
        val mainData = retrofitExecutor.makeRequest { apiService.getCurrentWeatherInfoByCityId(cityId) }.main!!
        val list = retrofitExecutor.makeRequest { apiService.getWeatherForecastByCityId(cityId) }.list!!
        return CityWeatherDto(cityId,mainData,list)
    }



}