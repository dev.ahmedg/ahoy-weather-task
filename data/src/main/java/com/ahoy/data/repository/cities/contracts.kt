package com.ahoy.data.repository.cities


import com.ahoy.data.network.entities.City
import com.ahoy.data.other.ResultState
import kotlinx.coroutines.flow.Flow


interface CitiesLocalDataSource {


    suspend fun addFavCity(city:City)

    suspend fun isFavCity(id:Int):Boolean

    suspend fun searchCities(query: String):List<City>

    suspend fun getFavCities():List<City>

}


interface CitiesRepo {

    fun searchCities(query: String): Flow<ResultState<List<City>>>

    fun getFavCities(): Flow<ResultState<List<City>>>

    suspend fun isFavCity(id: Int): Boolean

    suspend fun addFavCity(city: City): Flow<Boolean>

}