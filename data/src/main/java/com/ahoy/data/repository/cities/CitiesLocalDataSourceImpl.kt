package com.ahoy.data.repository.cities

import com.ahoy.base.utils.JsonAssetManager
import com.ahoy.data.keyValueStore.KeyValueStore
import com.ahoy.data.network.entities.City
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class CitiesLocalDataSourceImpl(private val keyValueStore: KeyValueStore, private val jsonAssetManager: JsonAssetManager) : CitiesLocalDataSource {


    override suspend fun addFavCity(city: City) {
        keyValueStore.addFavCity(city)
    }


    override suspend fun isFavCity(id: Int): Boolean {
        return keyValueStore.isFavCity(id)
    }

    override suspend fun searchCities(query: String): List<City> {
        val allCities = withContext(Dispatchers.IO){
            jsonAssetManager.readFileAsArray<City>("city.list.json")!!
        }
        return allCities.filter { it.name!!.contains(query,true) }
    }

    override suspend fun getFavCities(): List<City> {
        return keyValueStore.getFavCities()
    }


}