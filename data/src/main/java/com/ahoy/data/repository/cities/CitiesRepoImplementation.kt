package com.ahoy.data.repository.cities

import com.ahoy.data.network.entities.City
import com.ahoy.data.other.ResultState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class CitiesRepoImplementation(
    private val localDataSource: CitiesLocalDataSource,
) : CitiesRepo {


    override fun searchCities(query: String): Flow<ResultState<List<City>>> {
        return flow {
            emit(ResultState.Loading)
            val data = localDataSource.searchCities(query)
            emit(ResultState.Success(data))
        }
    }

    override fun getFavCities(): Flow<ResultState<List<City>>> {
        return flow {
            emit(ResultState.Loading)
            val data = localDataSource.getFavCities()
            emit(if (data.isEmpty())ResultState.Empty else ResultState.Success(data) )
        }
    }


    override suspend fun isFavCity(id: Int): Boolean {
        return localDataSource.isFavCity(id)
    }


    override suspend fun addFavCity(city: City):Flow<Boolean> {
        return flow {
            localDataSource.addFavCity(city)
            emit(true)
        }
    }


}