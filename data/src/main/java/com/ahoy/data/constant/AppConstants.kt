package com.ahoy.data.constant

import com.ahoy.data.BuildConfig


var BASE_API_URL = BuildConfig.BASE_API_URL
const val API_KEY = BuildConfig.API_KEY
const val DEFAULT_NETWORK_TIMEOUT = 100L




const val API_KEY_PARAM = "appid"

const val CURRENT_WEATHER_INFO_API = "weather"
const val DAILY_FORECAST_API = "forecast/daily"
