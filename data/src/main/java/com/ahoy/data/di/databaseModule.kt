package com.ahoy.data.di

import android.app.Application
import androidx.room.Room
import com.ahoy.data.database.AppDatabase
import com.ahoy.data.keyValueStore.KeyValueStore
import com.ahoy.data.keyValueStore.KeyValueStoreHawkImpl
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

/**
 * Koin module for room database
 */
val databaseModule = module {

    fun provideDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, "app_db")
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()
    }

    single { provideDatabase(androidApplication()) }

}