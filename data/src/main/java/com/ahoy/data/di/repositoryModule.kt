package com.ahoy.data.di


import com.ahoy.data.repository.cities.CitiesLocalDataSource
import com.ahoy.data.repository.cities.CitiesLocalDataSourceImpl
import com.ahoy.data.repository.cities.CitiesRepo
import com.ahoy.data.repository.cities.CitiesRepoImplementation
import com.ahoy.data.repository.weather.*
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module


/**
 * Koin module for repositories , remote/local data sources an other sources
 */
val  repositoryModule = module {

    factory<CitiesLocalDataSource>{ CitiesLocalDataSourceImpl(get(),get()) }

    factory<CitiesRepo>{ CitiesRepoImplementation(get()) }

    factory<WeatherLocalDataSource>{ WeatherLocalDataSourceImpl(get(),get()) }

    factory<WeatherRemoteDataSource>{ WeatherRemoteDataSourceImpl(get(),get()) }

    factory<WeatherRepo>{ WeatherRepoImplementation(androidContext(),get(),get(),get()) }

}