package com.ahoy.data.di



import com.ahoy.data.domian.*
import org.koin.dsl.module


val useCasesModule = module {

    factory { GetCityWeatherInfoUseCase(get()) }

    factory { GetCurrentWeatherInfoUseCase(get()) }

    factory { AddCityToFavUseCase(get()) }

    factory { SearchCitesUseCase(get()) }

    factory { GetFavCitesUseCase(get()) }

    factory { GetTempUnitUseCase(get()) }

    factory { SetTempUnitUseCase(get()) }

    factory { UpdateLastUserLocationUseCase(get()) }

    factory { GetLastUserLocationUseCase(get()) }

}