package com.ahoy.data.di

import com.ahoy.data.keyValueStore.KeyValueStore
import com.ahoy.data.keyValueStore.KeyValueStoreHawkImpl
import org.koin.dsl.module

/**
 * Koin module for room database
 */
val keyValueStoreModule = module {

    single<KeyValueStore> { KeyValueStoreHawkImpl() }

}