package com.ahoy.data.keyValueStore

import android.location.Location
import com.ahoy.data.keyValueStore.entity.CurrentWeatherDto
import com.ahoy.data.network.entities.City


/**
 * KeyValueStore protocol
 */
interface KeyValueStore {

    suspend fun addFavCity(model: City)

    suspend fun isFavCity(id:Int):Boolean

    fun getFavCities(): MutableList<City>

    suspend fun updateCurrentWeather(model: CurrentWeatherDto)

    fun getCurrentWeather(): CurrentWeatherDto?

    fun setTempUnitType(type: TempUnitType)

    fun getTempUnitType():TempUnitType

    fun setLastLocation(location: Location)

    fun getLastLocation():Location?

    companion object {
        const val KEY_FAVOURITES_CITIES= "fav_cities"
        const val KEY_CURRENT_WEATHER_INFO= "current_weather_info"
        const val KEY_TMP_UNIT_TYPE= "tmp_unit_type"
        const val KEY_USER_LAST_LOCATION= "last_location"
    }


}