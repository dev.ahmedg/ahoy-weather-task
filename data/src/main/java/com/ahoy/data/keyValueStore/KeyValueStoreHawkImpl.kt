package com.ahoy.data.keyValueStore

import android.location.Location
import com.ahoy.data.keyValueStore.KeyValueStore.Companion.KEY_CURRENT_WEATHER_INFO
import com.ahoy.data.keyValueStore.KeyValueStore.Companion.KEY_FAVOURITES_CITIES
import com.ahoy.data.keyValueStore.KeyValueStore.Companion.KEY_TMP_UNIT_TYPE
import com.ahoy.data.keyValueStore.KeyValueStore.Companion.KEY_USER_LAST_LOCATION
import com.ahoy.data.keyValueStore.entity.CurrentWeatherDto
import com.ahoy.data.network.entities.City
import com.orhanobut.hawk.Hawk


/**
 * KeyValueStore implementation with Hawk library
 */
class KeyValueStoreHawkImpl : KeyValueStore {


    override suspend fun addFavCity(model: City) {
        val list = getFavCities()
        if (list.find { it.id ==model.id } ==null){
            list.add(model)
            Hawk.put(KEY_FAVOURITES_CITIES,list)
        }
    }


    override suspend fun isFavCity(id: Int): Boolean {
        val list = getFavCities()
        return list.find { it.id ==id } !=null
    }

    override fun getFavCities(): MutableList<City> {
        return Hawk.get(KEY_FAVOURITES_CITIES,mutableListOf())
    }

    override suspend fun updateCurrentWeather(model: CurrentWeatherDto) {
        Hawk.put(KEY_CURRENT_WEATHER_INFO,model)
    }

    override fun getCurrentWeather(): CurrentWeatherDto? {
        return Hawk.get(KEY_CURRENT_WEATHER_INFO)
    }

    override  fun setTempUnitType(type: TempUnitType) {
        Hawk.put(KEY_TMP_UNIT_TYPE,type)
    }

    override  fun getTempUnitType(): TempUnitType {
        return Hawk.get(KEY_TMP_UNIT_TYPE,TempUnitType.Celsius)
    }

    override fun setLastLocation(location: Location) {
        Hawk.put(KEY_USER_LAST_LOCATION,location)
    }

    override fun getLastLocation(): Location? {
        return Hawk.get(KEY_USER_LAST_LOCATION)
    }


}