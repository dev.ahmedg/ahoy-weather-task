package com.ahoy.data.keyValueStore.entity

import com.ahoy.data.network.entities.ForecastItem
import com.ahoy.data.network.entities.WeatherMainData

open class CurrentWeatherDto(

    val weatherInfo: WeatherMainData,

    val forecasts: List<ForecastItem>

)
