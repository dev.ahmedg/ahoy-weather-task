package com.ahoy.data.keyValueStore

enum class TempUnitType(val value:String){
    Celsius("c"),Fahrenheit("f")
}