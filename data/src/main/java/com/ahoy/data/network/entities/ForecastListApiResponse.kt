package com.ahoy.data.network.entities

import com.google.gson.annotations.SerializedName

data class ForecastListApiResponse(

	@field:SerializedName("cnt")
	val cnt: Int? = null,

	@field:SerializedName("list")
	val list: List<ForecastItem>? = null
): BaseResponse()


data class ForecastItem(

	@field:SerializedName("sunrise")
	val sunrise: String? = null,

	@field:SerializedName("deg")
	val deg: String? = null,

	@field:SerializedName("pressure")
	val pressure: String? = null,

	@field:SerializedName("clouds")
	val clouds: String? = null,

	@field:SerializedName("speed")
	val speed: String? = null,

	@field:SerializedName("dt")
	val dt: String? = null,

	@field:SerializedName("pop")
	val pop: String? = null,

	@field:SerializedName("sunset")
	val sunset: String? = null,

	@field:SerializedName("humidity")
	val humidity: String? = null,

	@field:SerializedName("gust")
	val gust: String? = null,

	@field:SerializedName("rain")
	val rain: String? = null
)






