package com.ahoy.data.network.entities

import com.google.gson.annotations.SerializedName

open class BaseResponse(

	@field:SerializedName("cod")
	val cod: String? = null,

	@field:SerializedName("message")
	val message: String? = null
)
