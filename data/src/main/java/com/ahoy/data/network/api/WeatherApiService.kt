package com.ahoy.data.network.api



import com.ahoy.data.constant.CURRENT_WEATHER_INFO_API
import com.ahoy.data.constant.DAILY_FORECAST_API
import com.ahoy.data.network.entities.CurrentWeatherInfoApiResponse
import com.ahoy.data.network.entities.ForecastListApiResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApiService {

    @GET(CURRENT_WEATHER_INFO_API)
    suspend fun getCurrentWeatherInfoByCoord(@Query("lat") lat: Double,@Query("lon") lng: Double): Response<CurrentWeatherInfoApiResponse>

    @GET(CURRENT_WEATHER_INFO_API)
    suspend fun getCurrentWeatherInfoByCityId(@Query("id") id: Int): Response<CurrentWeatherInfoApiResponse>

    @GET(DAILY_FORECAST_API)
    suspend fun getWeatherForecastByCityId(@Query("id") id: Int): Response<ForecastListApiResponse>

    @GET(DAILY_FORECAST_API)
    suspend fun getWeatherForecastByCoord(@Query("lat") lat: Double,@Query("lon") lng: Double): Response<ForecastListApiResponse>


}