package com.ahoy.data.network.entities

import com.google.gson.annotations.SerializedName

data class CurrentWeatherInfoApiResponse(

	@field:SerializedName("visibility")
	val visibility: Int? = null,

	@field:SerializedName("timezone")
	val timezone: String? = null,

	@field:SerializedName("main")
	val main: WeatherMainData? = null,

	@field:SerializedName("dt")
	val dt: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("base")
	val base: String? = null

):BaseResponse()


data class WeatherMainData(

	@field:SerializedName("temp")
	val temp: String? = null,

	@field:SerializedName("temp_min")
	val tempMin: String? = null,

	@field:SerializedName("humidity")
	val humidity: String? = null,

	@field:SerializedName("pressure")
	val pressure: String? = null,

	@field:SerializedName("feels_like")
	val feelsLike: String? = null,

	@field:SerializedName("temp_max")
	val tempMax: String? = null
)


